package fastfood_pos.model;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.ClassOrderer;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestClassOrder;
import org.junit.jupiter.api.TestMethodOrder;

import jakarta.persistence.PersistenceException;

@TestMethodOrder(OrderAnnotation.class)
@TestClassOrder(ClassOrderer.OrderAnnotation.class)
public class OrderProductModelTest {
    private static SessionFactory sessionFactory;
    private static Session session;
    private static Long orderId;
    private static Long productId;

    @BeforeAll
    public static void setup() throws Exception {
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
            System.out.println("SessionFactory created");

        } catch (Exception e) {
            StandardServiceRegistryBuilder.destroy(registry);
            throw e;
        }
    }

    @AfterAll
    public static void tearDown() {
        if (sessionFactory != null) {
            sessionFactory.close();
        }
        System.out.println("SessionFactory destroyed");
    }

    @BeforeEach
    public void openSession() {
        session = sessionFactory.openSession();
        System.out.println("Session created");
    }

    @AfterEach
    public void closeSessionFactory() {
        if (session != null) {
            session.close();
        }
        System.out.println("Session closed\n");
    }

    @Nested
    @Order(1)
    @TestClassOrder(ClassOrderer.OrderAnnotation.class)
    class Positive_test_case {
        @Nested
        @Order(1)
        @TestMethodOrder(OrderAnnotation.class)
        class Product_CRUD_operation {

            @Test
            @Order(1)
            public void create() throws Exception {
                System.out.println("Running testCreate...");

                session.beginTransaction();

                ProductModel product = new ProductModel("baby clothes", "/hello/world/picture.jpg", 192.0);

                productId = (Long) session.save(product);

                session.getTransaction().commit();

                Assertions.assertTrue(productId > 0);
            }

            @Test
            @Order(2)
            public void get() throws Exception {
                System.out.println("Running testGet...");

                ProductModel expectedProduct = new ProductModel(productId, "baby clothes",
                        "/hello/world/picture.jpg", 192.0);
                ProductModel actualProduct = session.find(ProductModel.class, productId);

                Assertions.assertEquals(actualProduct.toString(), expectedProduct.toString());
            }

            @Test
            @Order(3)
            public void list() throws Exception {
                System.out.println("Running testList...");

                List<ProductModel> resultList = session
                        .createQuery("from %s".formatted(ProductModel.class.getSimpleName()), ProductModel.class)
                        .getResultList();

                Assertions.assertFalse(resultList.isEmpty());
            }

            @Test
            @Order(4)
            public void update() throws Exception {
                System.out.println("Running testUpdate...");

                ProductModel product = new ProductModel(productId, "baby clothes (edited)", "/hello/world/picture.jpg",
                        192.0);

                session.beginTransaction();
                session.update(product);
                session.getTransaction().commit();

                ProductModel updatedProduct = session.find(ProductModel.class, productId);

                Assertions.assertTrue(updatedProduct.equals(product));
            }

            @Test
            @Order(5)
            public void delete() throws Exception {
                System.out.println("Running testDelete...");

                ProductModel product = session.find(ProductModel.class, productId);

                session.beginTransaction();
                session.delete(product);
                session.getTransaction().commit();

                ProductModel deletedProduct = session.find(ProductModel.class, productId);

                Assertions.assertNull(deletedProduct);
            }
        }

        @Nested
        @Order(2)
        @TestMethodOrder(OrderAnnotation.class)
        class Order_CRUD_operation {
            @Test
            @Order(1)
            public void create() throws Exception {
                System.out.println("Running testCreate...");

                session.beginTransaction();

                ProductModel product = new ProductModel("/hello/world/picture.jpg", "baby clothes", 192.0);
                OrderModel order = new OrderModel(0.0, 0.1, 0.2, 0.3);

                productId = (Long) session.save(product);

                order.addProduct(product, 1);

                orderId = (Long) session.save(order);

                Assertions.assertTrue(orderId > 0);

                session.getTransaction().commit();
            }

            @Test
            @Order(2)
            public void get() throws Exception {
                System.out.println("Running testGet...");

                OrderModel expectedOrder = new OrderModel(orderId, 0.0, 0.1, 0.2, 0.3);
                OrderModel actualOrder = session.find(OrderModel.class, orderId);

                Assertions.assertEquals(actualOrder.toString(), expectedOrder.toString());
                Assertions.assertEquals(1, actualOrder.getOrderProducts().size());

                ArrayList<ProductModel> actualProducts = new ArrayList<ProductModel>();
                ArrayList<ProductModel> expectedProducts = new ArrayList<ProductModel>();

                for (OrderProductModel actualProduct : actualOrder.getOrderProducts()) {
                    actualProducts.add(actualProduct.getProduct());
                }

                expectedProducts
                        .add(new ProductModel(actualProducts.get(0).getId(), "/hello/world/picture.jpg", "baby clothes",
                                192.0));

                Assertions.assertEquals(actualProducts.get(0).toString(), expectedProducts.get(0).toString());

            }

            @Test
            @Order(3)
            public void list() throws Exception {
                System.out.println("Running testList...");

                Query<OrderModel> query = session.createQuery("from %s".formatted(OrderModel.class.getSimpleName()),
                        OrderModel.class);
                List<OrderModel> resultList = query.getResultList();

                Assertions.assertFalse(resultList.isEmpty());
            }

            @Test
            @Order(4)
            public void update() throws Exception {
                System.out.println("Running testUpdate...");

                OrderModel order = session.find(OrderModel.class, orderId);

                order.setTotalPrice(100.0);

                session.beginTransaction();
                session.merge(order);
                session.getTransaction().commit();

                OrderModel updatedOrder = session.find(OrderModel.class, orderId);

                Assertions.assertEquals(updatedOrder.toString(), order.toString());
            }

            @Test
            @Order(5)
            public void delete() throws Exception {
                System.out.println("Running testDelete...");

                OrderModel order = session.find(OrderModel.class, orderId);

                ArrayList<OrderProductModel> orderProducts = new ArrayList<OrderProductModel>(order.getOrderProducts());

                session.beginTransaction();
                session.delete(order);

                Assertions.assertNull(session.find(OrderModel.class, orderId));

                for (OrderProductModel orderProduct : orderProducts) {
                    Assertions.assertNull(session.find(OrderProductModel.class, orderProduct.getId()));
                    Assertions.assertNotNull(session.find(ProductModel.class, orderProduct.getProduct().getId()));
                    session.delete(orderProduct.getProduct()); // Clean up the database
                }

                session.getTransaction().commit();
            }

        }
    }

    @Nested
    @Order(2)
    @TestMethodOrder(OrderAnnotation.class)
    class Negative_test_case {

        @Test
        @Order(1)
        public void Order_with_an_unavailable_product() {
            System.out.println("Running Order_with_an_unavailable_product...");

            session.beginTransaction();

            ProductModel product = new ProductModel("/hello/world/picture.jpg", "baby clothes", 192.0);
            OrderModel order = new OrderModel(0.0, 0.1, 0.2, 0.3);

            order.addProduct(product, 1);
            session.save(order);

            Assertions.assertThrows(IllegalStateException.class, () -> {
                session.getTransaction().commit();
            });
        }

        @Test
        @Order(2)
        public void Delete_a_product_in_the_database_after_purchase() throws Exception {
            System.out.println("Running Delete_a_product_in_the_database_after_purchase...");

            ProductModel product = new ProductModel("/hello/world/picture.jpg", "baby clothes", 192.0);
            OrderModel order = new OrderModel(0.0, 0.1, 0.2, 0.3);

            session.beginTransaction();

            productId = (Long) session.save(product);
            Assertions.assertTrue(productId > 0);

            order.addProduct(product, 1);

            orderId = (Long) session.save(order);
            Assertions.assertTrue(orderId > 0);

            session.delete(product);

            Assertions.assertThrows(PersistenceException.class, () -> {
                session.getTransaction().commit();
            });
        }
    }
}
