package fastfood_pos.controller;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import fastfood_pos.model.OrderModel;
import fastfood_pos.model.ProductListSelectionModel;
import fastfood_pos.model.ProductModel;
import fastfood_pos.model.ShoppingCartTableModel;
import fastfood_pos.model.ShoppingCartTableRowModel;
import fastfood_pos.service.OrderService;
import fastfood_pos.service.ProductService;
import fastfood_pos.view.order_menu_tab.OrderMenuTab;
import fastfood_pos.view.order_menu_tab.PaymentSummaryView;
import fastfood_pos.view.order_menu_tab.ProductList;
import fastfood_pos.view.order_menu_tab.ProductListCell;
import fastfood_pos.view.order_menu_tab.ShoppingCartActionButton;
import fastfood_pos.view.order_menu_tab.ShoppingCartTable;
import lombok.Getter;

public class OrderMenuTabController {
    @Getter
    private OrderMenuTab view;
    private OrderService orderService;
    private ProductService productService;
    private List<ProductModel> products;
    private ShoppingCartTable shoppingCartTable;
    private ShoppingCartTableModel shoppingCartTableModel;
    private ProductList productList;
    private DefaultListModel<ProductListCell> productListModel;
    private DefaultListSelectionModel productListSelectionModel;
    private PaymentSummaryView paymentSummaryView;
    private PaymentSummaryController paymentSummaryController;
    private JTextField moneyInput;
    private JButton payOrConFirmOrFinishButton;
    private JButton addButton;
    private JButton decreseButton;
    private JButton removeButton;

    OrderMenuTabController(OrderMenuTab view) {
        this.view = view;
        orderService = new OrderService();
        productService = new ProductService();

        products = productService.findAll();

        shoppingCartTable = view.getShoppingCartTable();
        shoppingCartTableModel = shoppingCartTable.getModel();
        productList = view.getProductList();
        productListModel = productList.getModel();
        paymentSummaryView = view.getPaymentSummary();
        paymentSummaryController = new PaymentSummaryController(paymentSummaryView);
        moneyInput = paymentSummaryController.getMoneyInput();
        payOrConFirmOrFinishButton = paymentSummaryController.getPayOrConFirmOrFinishButton();

        initView();
        initController();
    }

    private void initView() {
        productList.addAllProducts(products);
    }

    private void initController() {
        ShoppingCartActionButton shoppingCartActionButton = view.getShoppingCartActionButton();
        addButton = shoppingCartActionButton.getAddButton();
        decreseButton = shoppingCartActionButton.getDecreaseButton();
        removeButton = shoppingCartActionButton.getRemoveButton();

        addButton.setEnabled(false);
        decreseButton.setEnabled(false);
        removeButton.setEnabled(false);
        payOrConFirmOrFinishButton.setEnabled(false);
        shoppingCartTable.setSelectionModel(new DefaultListSelectionModel() {
            @Override
            public void setSelectionInterval(int index0, int index1) {
                if (index0 == index1) {
                    if (isSelectedIndex(index0)) {
                        this.clearSelection();
                        productList.clearSelection();
                        return;
                    }
                }

                super.setSelectionInterval(index0, index0); // SINGLE_SELECTION

                Long productSelectedId = shoppingCartTableModel.getValueAt(index0).getProduct().getId();
                int productListSelectedIndex = productList.getSelectedIndex();

                if (productListSelectedIndex != -1) {
                    if (productSelectedId == productList.getSelectedValue().getProduct().getId()) {
                        return;
                    }
                }

                for (int index = 0, size = productListModel.size(); index < size; index++) {
                    if (productSelectedId == productListModel.get(index).getProduct().getId()) {
                        productList.setSelectedIndex(index);
                        productList.ensureIndexIsVisible(index);
                        break;
                    }
                }
            }

        });
        shoppingCartTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent event) {

                if (view.getShoppingCartTable().getSelectedRowCount() == 0) {
                    decreseButton.setEnabled(false);
                    removeButton.setEnabled(false);
                } else if (!decreseButton.isEnabled() && !removeButton.isEnabled()) {
                    decreseButton.setEnabled(true);
                    removeButton.setEnabled(true);
                }
            }
        });

        productListSelectionModel = new ProductListSelectionModel(ProductListSelectionModel.SINGLE_SELECTION) {

            @Override
            public void setSelectionMode(int SelectionMode) {
                this.SelectionMode = SelectionMode;
            }

            @Override
            public void setSelectionInterval(int index0, int index1) {
                if (this.SelectionMode == NO_SELECTION) {
                    return;
                }

                if (index0 == index1) {
                    if (isSelectedIndex(index0)) {
                        this.clearSelection();
                        shoppingCartTable.clearSelection();
                        return;
                    }
                }

                super.setSelectionInterval(index0, index0); // SINGLE_SELECTION

                Long productSelectedId = productList.getSelectedValue().getProduct().getId();
                int shoppingCartTableSelectedIndex = shoppingCartTable.getSelectedRow();

                if (shoppingCartTableSelectedIndex != -1) {
                    if (productSelectedId == shoppingCartTableModel.getValueAt(shoppingCartTableSelectedIndex)
                            .getProduct().getId()) {
                        return;
                    }
                    shoppingCartTable.clearSelection();
                }

                for (int index = 0, size = shoppingCartTableModel.getRowCount(); index < size; index++) {
                    if (productSelectedId == shoppingCartTableModel.getValueAt(index).getProduct().getId()) {
                        shoppingCartTable.setRowSelectionInterval(index, index);
                        shoppingCartTable.scrollRectToVisible(shoppingCartTable.getCellRect(index, 0, true));
                        break;
                    }
                }
            }
        };
        productList.setSelectionModel(productListSelectionModel);
        productList.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent event) {

                if (productList.getSelectedIndex() == -1) {
                    addButton.setEnabled(false);
                } else if (!addButton.isEnabled()) {
                    addButton.setEnabled(true);
                }
            }
        });

        moneyInput.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent event) {
                switch (event.getKeyCode()) {
                    case KeyEvent.VK_ENTER:
                        payOrConFirmOrFinishButton.doClick();
                        return;
                    case KeyEvent.VK_BACK_SPACE:
                    case KeyEvent.VK_DELETE:
                        moneyInput.setForeground(Color.BLACK);
                    default:
                        payOrConFirmOrFinishButton.setText("ชำระเงิน");
                        super.keyPressed(event);
                        if (paymentSummaryController.getChange() != 0.0) {
                            paymentSummaryController.setChange(0.0);
                        }
                }
            }
        });

        addButton.addActionListener(event -> addToCart());
        decreseButton.addActionListener(event -> decreaseProductQuantityFromCart());
        removeButton.addActionListener(event -> removeFromCart());
        payOrConFirmOrFinishButton.addActionListener(event -> payOrConFirmOrFinish());
    }

    private void addToCart() {
        ProductListCell productListCell = productList.getSelectedValue();
        if (productListCell == null) {
            return;
        }

        if (shoppingCartTableModel.add(productListCell.getProduct()) == ShoppingCartTableModel.PRODUCT_ADD) {
            int shoppingCartTableLastRow = shoppingCartTable.getRowCount() - 1;
            shoppingCartTable.setRowSelectionInterval(shoppingCartTableLastRow, shoppingCartTableLastRow);

            if (payOrConFirmOrFinishButton.isEnabled() == false) {
                payOrConFirmOrFinishButton.setEnabled(true);
            }
        }
        paymentSummaryController.add(productListCell.getProduct().getPrice());
        payOrConFirmOrFinishButton.setText("ชำระเงิน");
    }

    private void decreaseProductQuantityFromCart() {
        ProductListCell productListCell = productList.getSelectedValue();
        if (productListCell == null) {
            return;
        }

        if (shoppingCartTableModel.decrease(shoppingCartTable.getSelectedRow()) == ShoppingCartTableModel.PRODUCT_REMOVE
                && shoppingCartTableModel.getRowCount() == 0) {
            payOrConFirmOrFinishButton.setEnabled(false);
        }
        paymentSummaryController.decrease(productListCell.getProduct().getPrice());
        payOrConFirmOrFinishButton.setText("ชำระเงิน");
    }

    private void removeFromCart() {
        ProductListCell productListCell = productList.getSelectedValue();
        if (productListCell == null) {
            return;
        }

        ShoppingCartTableRowModel removedProduct = shoppingCartTableModel.remove(shoppingCartTable.getSelectedRow());
        paymentSummaryController.remove(removedProduct.getProduct().getPrice(), removedProduct.getQuantity());

        if (shoppingCartTableModel.getRowCount() == 0) {
            payOrConFirmOrFinishButton.setEnabled(false);

        }
        payOrConFirmOrFinishButton.setText("ชำระเงิน");
    }

    private void payOrConFirmOrFinish() {
        if (payOrConFirmOrFinishButton.getText().equals("เสร็จสิ้น")) {
            finishAndReset();
        } else if (payOrConFirmOrFinishButton.getText().equals("ยืนยัน")) {
            confirmPayment();
        } else {
            pay();
        }
    }

    private void pay() {
        if (moneyInput.getText().isEmpty()) {
            return;
        }

        Double moneyReceived;
        try {
            moneyReceived = Double.parseDouble(moneyInput.getText());
        } catch (NumberFormatException e) {
            moneyInput.setForeground(Color.RED);
            return;
        }

        Double netTotalPrice = paymentSummaryController.getNetTotalPrice();
        paymentSummaryController.setChange(moneyReceived - netTotalPrice);

        if (moneyReceived < netTotalPrice) {
            moneyInput.setForeground(Color.RED);
            return;
        }

        payOrConFirmOrFinishButton.setText("ยืนยัน");
    }

    private void confirmPayment() {
        OrderModel order = new OrderModel();
        for (ShoppingCartTableRowModel shoppingCartTableRow : shoppingCartTableModel.toList()) {
            order.addProduct(shoppingCartTableRow.getProduct(), shoppingCartTableRow.getQuantity());
        }
        order.setTotalPrice(paymentSummaryController.getTotalPrice());
        order.setDiscount(paymentSummaryController.getDiscount());
        order.setVat(paymentSummaryController.getVat());
        order.setNetTotalPrice(paymentSummaryController.getNetTotalPrice());
        saveOrder(order);

        shoppingCartTable.setRowSelectionAllowed(false);
        productListSelectionModel.clearSelection();
        productListSelectionModel.setSelectionMode(ProductListSelectionModel.NO_SELECTION);
        moneyInput.setEnabled(false);
        payOrConFirmOrFinishButton.setText("เสร็จสิ้น");
        addButton.setEnabled(false);
        decreseButton.setEnabled(false);
        removeButton.setEnabled(false);
    }

    private void finishAndReset() {
        shoppingCartTableModel.removeAllElements();
        shoppingCartTable.setRowSelectionAllowed(true);
        productListSelectionModel.setSelectionMode(ProductListSelectionModel.SINGLE_SELECTION);
        paymentSummaryController.clear();
        moneyInput.setEnabled(true);
        payOrConFirmOrFinishButton.setText("ชำระเงิน");
    }

    public void saveOrder(OrderModel order) {
        orderService.save(order);
    }
}
