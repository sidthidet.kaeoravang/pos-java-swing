package fastfood_pos.controller;

import javax.swing.JButton;

import fastfood_pos.model.OrderModel;
import fastfood_pos.view.MainWindow;
import fastfood_pos.view.order_menu_tab.OrderMenuTab;
import fastfood_pos.view.sales_summary_tab.SalesSummaryTab;

public class MainWindowController {
    private MainWindow view;
    private JButton orderMenuButton;
    private JButton salesSummaryButton;

    private OrderMenuTabController orderMenuTabController;
    private SalesSummaryTabController salesSummaryTabController;

    private String orderMenuTab = "orderMenuTab";
    private String salesSummaryTab = "salesSummaryTab";

    public MainWindowController(MainWindow view) {
        this.view = view;
        salesSummaryTabController = new SalesSummaryTabController(new SalesSummaryTab());
        orderMenuTabController = new OrderMenuTabController(new OrderMenuTab()) {
            @Override
            public void saveOrder(OrderModel order) {
                super.saveOrder(order);
                salesSummaryTabController.addOrder(order);
            }
        };

        orderMenuButton = view.getOrderMenuButton();
        salesSummaryButton = view.getSalesSummaryButton();
        
        initView();
        initController();
    }

    private void initView() {
        view.addBody(orderMenuTabController.getView(), orderMenuTab);
        view.addBody(salesSummaryTabController.getView(), salesSummaryTab);
        view.setSelectedBody(orderMenuTab);
        view.setVisible(true);
    }

    private void initController() {
        orderMenuButton.addActionListener(event -> moveToOrderMenuTab());
        salesSummaryButton.addActionListener(event -> moveToSalesSummaryTab());
    }

    private void moveToOrderMenuTab() {
        view.setSelectedBody(orderMenuTab);
    }

    private void moveToSalesSummaryTab() {
        view.setSelectedBody(salesSummaryTab);
    }
}
