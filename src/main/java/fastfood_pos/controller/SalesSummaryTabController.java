package fastfood_pos.controller;

import fastfood_pos.model.OrderModel;
import fastfood_pos.model.OrderProductModel;
import fastfood_pos.model.ProductModel;
import fastfood_pos.model.SummaryProductTableModel;
import fastfood_pos.model.SummaryProductTableRowModel;
import fastfood_pos.service.OrderProductService;
import fastfood_pos.service.OrderService;
import fastfood_pos.service.ProductService;
import fastfood_pos.view.sales_summary_tab.SalesSummaryPanel;
import fastfood_pos.view.sales_summary_tab.SalesSummaryTab;
import lombok.Getter;

public class SalesSummaryTabController {
    @Getter
    SalesSummaryTab view;
    private SalesSummaryPanel salesSummary;
    private SummaryProductTableModel productSummaryTableModel;
    private ProductService productService;
    private OrderService orderService;
    private OrderProductService orderProductService;

    public SalesSummaryTabController(SalesSummaryTab view) {
        this.view = view;
        salesSummary = view.getSalesSummary();
        productSummaryTableModel = view.getProductSummaryTableModel();
        productService = new ProductService();
        orderService = new OrderService();
        orderProductService = new OrderProductService();

        initView();
    }

    private void initView() {
        Double totalSales = 0.0;
        Double discount = 0.0;
        Double vat;
        Double revenue;

        for (OrderModel order : orderService.findAll()) {
            totalSales += order.getTotalPrice();
            discount += order.getDiscount();
        }
        vat = (totalSales - discount) * 0.07;
        revenue = totalSales - discount + vat;

        salesSummary.setTotalSales(totalSales);
        salesSummary.setDiscount(discount);
        salesSummary.setVat(vat);
        salesSummary.setRevenue(revenue);

        for (ProductModel product : productService.findAll()) {
            productSummaryTableModel.add(new SummaryProductTableRowModel(product,
                    orderProductService.getProductQuantityByProductId(product.getId())));
        }
    }

    public void addOrder(OrderModel order) {
        Double totalSales = salesSummary.getTotalSales();
        Double discount = salesSummary.getDiscount();
        Double vat;
        Double revenue;

        totalSales += order.getTotalPrice();
        discount += order.getDiscount();

        vat = (totalSales - discount) * 0.07;
        revenue = totalSales - discount + vat;

        salesSummary.setTotalSales(totalSales);
        salesSummary.setDiscount(discount);
        salesSummary.setVat(vat);
        salesSummary.setRevenue(revenue);

        SummaryProductTableRowModel summaryProductTableRowModel;
        for (OrderProductModel orderProductModel : order.getOrderProducts()) {
            summaryProductTableRowModel = productSummaryTableModel.getProduct(orderProductModel.getProduct().getId());
            summaryProductTableRowModel.addQuantity(orderProductModel.getQuantity());
            productSummaryTableModel.updateLastGettedProduct();
        }
    }

}
