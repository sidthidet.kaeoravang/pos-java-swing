package fastfood_pos.controller;

import javax.swing.JButton;
import javax.swing.JTextField;

import fastfood_pos.configuration.Color;
import fastfood_pos.view.TitleAndMoneyValuePanel;
import fastfood_pos.view.order_menu_tab.PaymentSummaryView;
import lombok.Getter;

public class PaymentSummaryController {
    private TitleAndMoneyValuePanel totalPrice;
    private TitleAndMoneyValuePanel discount;
    private TitleAndMoneyValuePanel vat;
    private TitleAndMoneyValuePanel netTotalPrice;
    private TitleAndMoneyValuePanel change;
    
    @Getter
    private JTextField moneyInput;
    @Getter
    private JButton payOrConFirmOrFinishButton;

    public PaymentSummaryController(PaymentSummaryView view) {
        totalPrice = view.getTotalPrice();
        discount = view.getDiscount();
        vat = view.getVat();
        netTotalPrice = view.getNetTotalPrice();
        change = view.getChange();
        moneyInput = view.getMoneyInput();
        payOrConFirmOrFinishButton = view.getPayOrConFirmOrFinishButton();
    }

    private void nextCalculate(Double price, Integer quantity) {
        Double totalPrice = this.totalPrice.getValue() + price * quantity;
        Double discount = totalPrice * 0.10;
        Double vat = (totalPrice - discount) * 0.07;
        Double netTotalPrice  = totalPrice - discount + vat;

        this.totalPrice.setValue(totalPrice);
        this.discount.setValue(discount);
        this.vat.setValue(vat);
        this.netTotalPrice.setValue(netTotalPrice);

        if (moneyInput.getText().isEmpty()) {
            return;
        }

        Double moneyReceived;
        try {
            moneyReceived = Double.parseDouble(moneyInput.getText());
        } catch (NumberFormatException e) {
            moneyInput.setForeground(Color.PAYMENT_MONEY_INPUT_ERROR_FOREGROUND);
            setChange(0.0);
            return;
        }

        if (moneyReceived < netTotalPrice) {
            moneyInput.setForeground(Color.PAYMENT_MONEY_INPUT_ERROR_FOREGROUND);
        }
        else {
            moneyInput.setForeground(Color.PAYMENT_MONEY_INPUT_FORGROUND);
        }

        setChange(moneyReceived - netTotalPrice);
    }

    public void add(Double price) {
        nextCalculate(price , 1);
    }

    public void decrease(Double price) {
        nextCalculate(price , -1);
    }

    public void remove(Double price, Integer quantity) {
        nextCalculate(price , -1 * quantity);
    }

    public void clear() {
        this.totalPrice.setValue(0.0);
        this.discount.setValue(0.0);
        this.vat.setValue(0.0);
        this.netTotalPrice.setValue(0.0);
        this.change.setValue(0.0);
        this.moneyInput.setText("");
    }

    public Double getTotalPrice() {
        return totalPrice.getValue();
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice.setValue(totalPrice);;
    }

    public Double getDiscount() {
        return discount.getValue();
    }

    public void setDiscount(Double discount) {
        this.discount.setValue(discount);
    }

    public Double getVat() {
        return vat.getValue();
    }

    public void setVat(Double vat) {
        this.vat.setValue(vat);
    }

    public Double getNetTotalPrice() {
        return netTotalPrice.getValue();
    }

    public void setNetTotalPrice(Double netTotalPrice) {
        this.netTotalPrice.setValue(netTotalPrice);
    }

    public Double getChange() {
        return change.getValue();
    }

    public void setChange(Double change) {
        this.change.setValue(change);
    }
    
}
