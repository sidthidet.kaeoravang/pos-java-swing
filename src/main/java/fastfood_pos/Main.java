package fastfood_pos;

import fastfood_pos.controller.MainWindowController;
import fastfood_pos.model.ProductModel;
import fastfood_pos.service.ProductService;
import fastfood_pos.view.MainWindow;

public class Main {
    public static void main(String[] args) {
        ProductService productService = new ProductService();
        if (productService.findAll().size() <= 0) {
            productService.save(new ProductModel("ชาวาม่าไก่", "images/chicken_shawarma.jpg", 50.0));
            productService.save(new ProductModel("ชูโรส", "images/churros.jpg", 40.0));
            productService.save(new ProductModel("เฟรนช์ฟรายส์", "images/french_fries.jpg", 30.0));
            productService.save(new ProductModel("แฮมเบอร์เกอร์", "images/hamburger.jpg", 45.0));
            productService.save(new ProductModel("ฮอทดอก", "images/hot_dog.jpg", 20.0));
            productService.save(new ProductModel("ไอศกรีม", "images/ice_cream.jpg", 25.0));
            productService.save(new ProductModel("พิซซ่า", "images/pizza.jpg", 100.0));
            productService.save(new ProductModel("ชาวาม่า", "images/shawarma.jpg", 50.0));
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainWindowController(new MainWindow("Fast Food"));
            }
        });
    }
}
