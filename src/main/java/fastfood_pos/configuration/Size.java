package fastfood_pos.configuration;

import java.awt.Dimension;

public class Size {
        public static final int WINDOW_RESOLUTION_DEFAULT_WIDTH = 1280;
        public static final int WINDOW_RESOLUTION_DEFAULT_HEIGHT = 800;

        public static final int TAB_ACTION_BUTTON_WIDTH = 135;
        public static final int TAB_ACTION_BUTTON_HEIGHT = 45;
        public static final Dimension TAB_ACTION_BUTTON = new Dimension(TAB_ACTION_BUTTON_WIDTH,
                        TAB_ACTION_BUTTON_HEIGHT);
        public static final int TAB_ACTION_ACTTION_BUTTON_CORNER_RADIUS = 20;

        public static final int ORDER_PRODUCT_ACTTION_BUTTON_WIDTH = 124;
        public static final int ORDER_PRODUCT_ACTTION_BUTTON_HEIGHT = 45;
        public static final Dimension ORDER_PRODUCT_ACTTION_BUTTON = new Dimension(
                        ORDER_PRODUCT_ACTTION_BUTTON_WIDTH,
                        ORDER_PRODUCT_ACTTION_BUTTON_HEIGHT);
        public static final int ORDER_PRODUCT_ACTTION_BUTTON_CORNER_RADIUS = 20;

        public static final int PRODUCT_LIST_CELL_WIDTH = 225;
        public static final int PRODUCT_LIST_CELL_HEIGHT = 188;
        public static final Dimension PRODUCT_LIST_CELL = new Dimension(PRODUCT_LIST_CELL_WIDTH,
                        PRODUCT_LIST_CELL_HEIGHT);

        public static final int SUMMARY_PRODUCT_TABLE_CELL_WIDTH = 217;
        public static final int SUMMARY_PRODUCT_TABLE_CELL_HEIGHT = 153;
}
