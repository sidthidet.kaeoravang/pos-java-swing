package fastfood_pos.configuration;

public class Color {
    public static final java.awt.Color BASE0 = new java.awt.Color(0x2c2c2c);
    public static final java.awt.Color BASE1 = new java.awt.Color(0xd9d9d9);
    public static final java.awt.Color BASE2 = java.awt.Color.WHITE;

    public static final java.awt.Color HEADER_BACKGROUND = BASE0;
    public static final java.awt.Color MENU_BUTTON_BACKGROUND = new java.awt.Color(0xf8f8f8);
    public static final java.awt.Color MENU_BUTTON_FOREGROUND = BASE2;

    public static final java.awt.Color BODY_BACKGROUND = BASE2;
    public static final java.awt.Color BODER_LINE = new java.awt.Color(0xb3b3b3);
    public static final java.awt.Color BODY_FOREGROUND = new java.awt.Color(0x383838);

    public static final java.awt.Color PRODUCT_LIST_CELL_TEXT = java.awt.Color.BLACK;
    public static final java.awt.Color PRODUCT_LIST_BORDER_CELL = new java.awt.Color(0xe5e5e5);
    public static final java.awt.Color PRODUCT_LIST_BORDER_SELECTED_CELL = new java.awt.Color(0x18a0fb);

    public static final java.awt.Color MODERN_SCROLL_PANE_BACKGROUND = BASE2;
    public static final java.awt.Color MODERN_SCROLL_PANE_FOREGROUND = BASE0;

    public static final java.awt.Color SHOPPING_CART_TABLE_HEADER_BACKGROUND = BASE0;
    public static final java.awt.Color SHOPPING_CART_TABLE_HEADER_FOREGROUND =  BASE1;

    public static final java.awt.Color SHOPPING_CART_ACTION_BUTTON_BACKGROUND = BASE0;
    public static final java.awt.Color SHOPPING_CART_ACTION_BUTTON_FOREGROUND = BASE1;

    public static final java.awt.Color PAYMENT_MONEY_INPUT_ERROR_FOREGROUND = java.awt.Color.RED;
    public static final java.awt.Color PAYMENT_MONEY_INPUT_FORGROUND = java.awt.Color.BLACK;

    public static final java.awt.Color SALES_SUMMARY_BACKGROUND = BASE2;

    public static final java.awt.Color SUMMARY_PRODUCT_TABLE_HEAD_BACKGROUND = BASE0;
    public static final java.awt.Color SUMMARY_PRODUCT_TABLE_HEAD_FOREGROUND = BASE1;

}
