package fastfood_pos.configuration;

import java.awt.GraphicsEnvironment;
import java.io.InputStream;

public class Font {
        public static java.awt.Font TH_SARABUN;
        public static java.awt.Font TH_SARABUN_NEW;
        public static java.awt.Font TH_SARABUN_NEW_BOLD;

        static {
                try {
                        GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
                        InputStream THSarabunStream = ClassLoader.getSystemResourceAsStream("fonts/THSarabun.ttf");
                        InputStream THSarabunNewStream = ClassLoader.getSystemResourceAsStream("fonts/THSarabunNew.ttf");
                        InputStream THSarabunNewBoldStream = ClassLoader.getSystemResourceAsStream("fonts/THSarabunNew Bold.ttf");

                        TH_SARABUN = java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT, THSarabunStream);
                        TH_SARABUN_NEW = java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT, THSarabunNewStream);
                        TH_SARABUN_NEW_BOLD = java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT, THSarabunNewBoldStream);

                        graphicsEnvironment.registerFont(TH_SARABUN);
                        graphicsEnvironment.registerFont(TH_SARABUN_NEW);
                        graphicsEnvironment.registerFont(TH_SARABUN_NEW_BOLD);
                } catch (Exception e) {
                        e.printStackTrace();
                        TH_SARABUN = new java.awt.Font(null, java.awt.Font.PLAIN, 15);
                        TH_SARABUN_NEW = TH_SARABUN;
                        TH_SARABUN_NEW_BOLD = TH_SARABUN;
                }

        }
        public static final java.awt.Font MAIN_ACTTION_BUTTON = TH_SARABUN_NEW_BOLD.deriveFont(25f);
        public static final java.awt.Font SHOPPING_CART_TABLE_HEADER = TH_SARABUN.deriveFont(23f);
        public static final java.awt.Font SHOPPING_CART_TABLE_BODY = TH_SARABUN_NEW.deriveFont(22f);
        public static final java.awt.Font SHOPPING_CART_ACTTION_BUTTON = TH_SARABUN_NEW_BOLD.deriveFont(25f);
        public static final java.awt.Font PAYMENT_SUMMARY_ROW = TH_SARABUN_NEW_BOLD.deriveFont(22f);
        public static final java.awt.Font PRODUCT_LIST = TH_SARABUN_NEW.deriveFont(22f);

        public static final java.awt.Font TITLE_AND_MONEY_VALUE_PANEL = TH_SARABUN_NEW_BOLD.deriveFont(22f);

        public static final java.awt.Font SUMMARY_PRODUCT_TABLE_HEADER = TH_SARABUN.deriveFont(23f);
        public static final java.awt.Font SUMMARY_PRODUCT_TABLE_BODY = TH_SARABUN_NEW.deriveFont(22f);
}
