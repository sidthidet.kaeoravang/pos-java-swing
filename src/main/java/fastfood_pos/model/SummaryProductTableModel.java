package fastfood_pos.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.table.AbstractTableModel;

public class SummaryProductTableModel extends AbstractTableModel {

    protected static final String COLUMN_NAMES[] = { "สินค้า", "ชื่อ", "จำนวน", "ราคาต่อหน่วย(บาท)", "ยอดขาย(บาท)" };
    protected static final Class<?> COLUMN_TYPES[] = { JLabel.class, String.class, Integer.class, Double.class,
            Double.class };

    List<SummaryProductTableRowModel> summaryProductTableRows;
    int lastGettedProduct = -1;

    public SummaryProductTableModel() {
        summaryProductTableRows = new ArrayList<SummaryProductTableRowModel>();
    }

    public void add(SummaryProductTableRowModel summaryProductTableRow) {
        summaryProductTableRows.add(summaryProductTableRow);
        fireTableRowsInserted(summaryProductTableRows.size() - 1, summaryProductTableRows.size() - 1);
    }

    public SummaryProductTableRowModel getProduct(Long productId) {
        for (SummaryProductTableRowModel summaryProductTableRow : summaryProductTableRows) {
            lastGettedProduct++;
            if (productId == summaryProductTableRow.getId()) {
                return summaryProductTableRow;
            }
        }
        lastGettedProduct = -1;
        return null;
    }

    public void updateLastGettedProduct() {
        if (summaryProductTableRows.size() == 0 || lastGettedProduct == -1) {
            return;
        }

        fireTableRowsUpdated(lastGettedProduct, lastGettedProduct);
    }

    public SummaryProductTableRowModel remove(int rowIndex) {
        fireTableRowsDeleted(rowIndex, rowIndex);
        return summaryProductTableRows.remove(rowIndex);
    }

    public void removeAllElements() {
        if (summaryProductTableRows.size() == 0) {
            return;
        }
        fireTableRowsDeleted(0, summaryProductTableRows.size() - 1);
        summaryProductTableRows.removeAll(summaryProductTableRows);
    }

    @Override
    public int getRowCount() {
        return summaryProductTableRows.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return COLUMN_TYPES[columnIndex];
    }

    @Override
    public String getColumnName(int columnIndex) {
        return COLUMN_NAMES[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        SummaryProductTableRowModel rowValue = summaryProductTableRows.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return rowValue.getImage();

            case 1:
                return rowValue.getName();

            case 2:
                return rowValue.getQuantity();

            case 3:
                return rowValue.getUnitPrice();

            case 4:
                return rowValue.getSales();

            default:
                return null;
        }
    }

}
