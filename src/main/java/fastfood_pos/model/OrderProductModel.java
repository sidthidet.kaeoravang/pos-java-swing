package fastfood_pos.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "_OrderProduct")
@NoArgsConstructor
@Getter
@Setter
public class OrderProductModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    public OrderProductModel(Integer quantity, OrderModel order, ProductModel product) {
        this.quantity = quantity;
        this.order = order;
        this.product = product;
    }

    @Column(nullable = false)
    private Integer quantity;

    @ManyToOne
    @JoinColumn(name = "orderId", referencedColumnName = "id")
    private OrderModel order;

    @ManyToOne
    @JoinColumn(name = "productId", referencedColumnName = "id")
    private ProductModel product;
}
