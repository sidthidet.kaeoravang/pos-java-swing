package fastfood_pos.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class ShoppingCartTableModel extends AbstractTableModel {
    protected static final String COLUMN_NAMES[] = { "สินค้า", "จำนวน", "ราคารวม(บาท)" };
    protected static final Class<?> COLUMN_TYPES[] = { String.class, Integer.class, Double.class };

    public static final int PRODUCT_ADD = 0;
    public static final int PRODUCT_INCREASE = 1;
    public static final int PRODUCT_DECREASE = 2;
    public static final int PRODUCT_REMOVE = 3;

    private List<ShoppingCartTableRowModel> shoppingCartTableRows;

    public ShoppingCartTableModel() {
        shoppingCartTableRows = new ArrayList<ShoppingCartTableRowModel>();
    }

    public int add(ProductModel product) {

        for (int rowIndex = 0, tableRowCount = shoppingCartTableRows.size(); rowIndex < tableRowCount; rowIndex++) {
            if (shoppingCartTableRows.get(rowIndex).getProduct().getId() == product.getId()) {
                shoppingCartTableRows.get(rowIndex).increaseQuantity();
                fireTableRowsUpdated(rowIndex, rowIndex);
                return PRODUCT_INCREASE;
            }
        }
        shoppingCartTableRows.add(new ShoppingCartTableRowModel(product, 1));
        fireTableRowsInserted(shoppingCartTableRows.size() - 1, shoppingCartTableRows.size() - 1);
        return PRODUCT_ADD;
    }

    public int decrease(int rowIndex) {
        if (shoppingCartTableRows.get(rowIndex).getQuantity() <= 1) {
            shoppingCartTableRows.remove(rowIndex);
            fireTableRowsDeleted(rowIndex, rowIndex);
            return PRODUCT_REMOVE;
        }
        shoppingCartTableRows.get(rowIndex).decreaseQuantity();
        fireTableRowsUpdated(rowIndex, rowIndex);
        return PRODUCT_DECREASE;
    }

    public ShoppingCartTableRowModel remove(int rowIndex) {
        fireTableRowsDeleted(rowIndex, rowIndex);
        return shoppingCartTableRows.remove(rowIndex);
    }

    public void removeAllElements() {
        if (shoppingCartTableRows.size() == 0) {
            return;
        }
        fireTableRowsDeleted(0, shoppingCartTableRows.size() - 1);
        shoppingCartTableRows.removeAll(shoppingCartTableRows);
    }

    public ShoppingCartTableRowModel getValueAt(int rowIndex) {
        return shoppingCartTableRows.get(rowIndex);
    }

    public List<ShoppingCartTableRowModel> toList() {
        return shoppingCartTableRows;
    }

    @Override
    public int getRowCount() {
        return shoppingCartTableRows.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return COLUMN_TYPES[columnIndex];
    }

    @Override
    public String getColumnName(int columnIndex) {
        return COLUMN_NAMES[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ShoppingCartTableRowModel rowValue = getValueAt(rowIndex);
        switch (columnIndex) {
            case 0:
                return rowValue.getProduct().getName();
            case 1:
                return rowValue.getQuantity();
            case 2:
                return rowValue.getTotalPrice();
            default:
                return null;
        }
    }
}
