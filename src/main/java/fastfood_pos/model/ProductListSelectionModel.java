package fastfood_pos.model;

import javax.swing.DefaultListSelectionModel;

public class ProductListSelectionModel extends DefaultListSelectionModel {
    public static int NO_SELECTION = 0;
    public static int SINGLE_SELECTION = 1;
    protected int SelectionMode;

    public ProductListSelectionModel(int SelectionMode) {
        this.SelectionMode = SelectionMode;
    }
}
