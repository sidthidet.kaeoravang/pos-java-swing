package fastfood_pos.model;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import fastfood_pos.configuration.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SummaryProductTableRowModel {
    private Long id;
    private JLabel image;
    private String name;
    private Integer quantity;
    private Double unitPrice;

    public SummaryProductTableRowModel(ProductModel product, Integer quantity) {
        this.id = product.getId();
        setImage(product.getImagePath());
        this.name = product.getName();
        this.quantity = quantity;
        this.unitPrice = product.getPrice();
    }

    public void setImage(String imagePath) {
        if (image == null) {
            image = new JLabel();
        }
        image.setIcon(createImageIcon(imagePath));
    }

    private static ImageIcon createImageIcon(String path) {
        URL imageURL = ClassLoader.getSystemResource(path);
        if (imageURL == null) {
            System.err.println(String.format("'%s': No such file or directory", path));
            return null;
        }

        try {
            BufferedImage image = ImageIO.read(imageURL.openStream());
            Image imageNewSize = image.getScaledInstance(Size.SUMMARY_PRODUCT_TABLE_CELL_WIDTH,
                    Size.SUMMARY_PRODUCT_TABLE_CELL_HEIGHT, Image.SCALE_SMOOTH);
            return new ImageIcon(imageNewSize);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void addQuantity(Integer quantity) {
        this.quantity += quantity;
    }

    public Double getSales() {
        return quantity * unitPrice;
    }
}
