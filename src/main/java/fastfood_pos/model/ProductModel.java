package fastfood_pos.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "Product")
@NoArgsConstructor
@Getter
@Setter
@ToString
public class ProductModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String imagePath;

    @Column(nullable = false)
    private Double price;

    public ProductModel(String name, String imagePath, Double price) {
        this.name = name;
        this.imagePath = imagePath;
        this.price = price;
    }

    public ProductModel(Long id, String name, String imagePath, Double price) {
        this.id = id;
        this.name = name;
        this.imagePath = imagePath;
        this.price = price;
    }
}