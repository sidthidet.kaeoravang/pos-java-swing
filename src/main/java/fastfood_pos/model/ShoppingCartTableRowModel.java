package fastfood_pos.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShoppingCartTableRowModel {
    private ProductModel product;
    private Integer quantity;
    private Double totalPrice;

    public ShoppingCartTableRowModel(ProductModel product, Integer quantity) {
        this.product = product;
        this.quantity = quantity;
        this.totalPrice = product.getPrice();
    }

    public void increaseQuantity() {
        quantity++;
        totalPrice += product.getPrice();
    }

    public void decreaseQuantity() {
        quantity--;
        totalPrice -= product.getPrice();
    }
}
