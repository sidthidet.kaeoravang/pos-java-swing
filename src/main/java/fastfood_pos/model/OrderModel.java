package fastfood_pos.model;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "_Order")
@NoArgsConstructor
@Getter
@Setter
public class OrderModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Double totalPrice;

    @Column(nullable = false)
    private Double vat;

    @Column(nullable = false)
    private Double discount;

    @Column(nullable = false)
    private Double netTotalPrice;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Set<OrderProductModel> orderProducts = new HashSet<OrderProductModel>();

    public OrderModel(Double totalPrice, Double vat, Double discount, Double netTotalPrice) {
        this.totalPrice = totalPrice;
        this.vat = vat;
        this.discount = discount;
        this.netTotalPrice = netTotalPrice;
    }

    public OrderModel(Long id, Double totalPrice, Double vat, Double discount, Double netTotalPrice) {
        this.id = id;
        this.totalPrice = totalPrice;
        this.vat = vat;
        this.discount = discount;
        this.netTotalPrice = netTotalPrice;
    }

    public void addProduct(ProductModel product, Integer quantity) {
        this.orderProducts.add(new OrderProductModel(quantity, this, product));
    }

    @Override
    public String toString() {
        return "OrderModel [discount=" + discount + ", id=" + id + ", netTotalPrice=" + netTotalPrice + ", totalPrice="
                + totalPrice + ", vat=" + vat + "]";
    }
}
