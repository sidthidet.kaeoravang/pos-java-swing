package fastfood_pos.service;

import java.util.List;

import org.hibernate.Session;

import fastfood_pos.model.OrderModel;

public class OrderService extends BaseService {
    private Session session;

    public List<OrderModel> findAll() {
        session = getSessionFactory().openSession();
        List<OrderModel> resultList = session
                .createQuery("FROM %s".formatted(OrderModel.class.getSimpleName()), OrderModel.class)
                .getResultList();
        session.close();
        return resultList;
    }

    public void save(OrderModel order) {
        session = getSessionFactory().openSession();
        session.beginTransaction();
        session.save(order);
        session.getTransaction().commit();
        session.close();
    }
}
