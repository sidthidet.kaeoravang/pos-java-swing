package fastfood_pos.service;

import java.util.List;

import org.hibernate.Session;

import fastfood_pos.model.OrderProductModel;

public class OrderProductService extends BaseService {
    private Session session;

    public Integer getProductQuantityByProductId(Long productId) {
        String hql = "FROM " + OrderProductModel.class.getSimpleName() + " WHERE productId = :productId";
        List<OrderProductModel> orderProductListByProductId;

        session = getSessionFactory().openSession();
        try {
            orderProductListByProductId = session.createQuery(hql, OrderProductModel.class)
                    .setParameter("productId", productId).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            session.close();
            return -1;
        }

        if (orderProductListByProductId == null) {
            session.close();
            return 0;
        }

        Integer productQuantityCount = 0;
        for (OrderProductModel orderProduct : orderProductListByProductId) {
            productQuantityCount += orderProduct.getQuantity();
        }
        session.close();
        return productQuantityCount;
    }
}
