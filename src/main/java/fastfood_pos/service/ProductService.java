package fastfood_pos.service;

import java.util.List;

import org.hibernate.Session;

import fastfood_pos.model.ProductModel;

public class ProductService extends BaseService {
    private Session session;

    public List<ProductModel> findAll() {
        session = getSessionFactory().openSession();
        List<ProductModel> resultList = session
                .createQuery("FROM %s".formatted(ProductModel.class.getSimpleName()), ProductModel.class)
                .getResultList();
        session.close();
        return resultList;
    }

    public Long save(ProductModel product) {
        Long id;
        session = getSessionFactory().openSession();
        session.beginTransaction();
        id = (Long) session.save(product);
        session.getTransaction().commit();
        session.close();
        return id;
    }

}
