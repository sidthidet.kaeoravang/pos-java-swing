package fastfood_pos.view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

import fastfood_pos.configuration.Color;
import fastfood_pos.configuration.Font;
import fastfood_pos.configuration.Size;
import lombok.Getter;

@Getter
public class MainWindow extends JFrame {
    private Button orderMenuButton;
    private Button salesSummaryButton;
    private JPanel body;
    private CardLayout bodyLayoutManager;

    public MainWindow(String title) {

        orderMenuButton = new Button("เมนูสั่งซื้อ", Font.MAIN_ACTTION_BUTTON, Size.TAB_ACTION_BUTTON);
        orderMenuButton.setBackground(Color.MENU_BUTTON_BACKGROUND);

        salesSummaryButton = new Button("ดูสรุปยอดขาย", Font.MAIN_ACTTION_BUTTON, Size.TAB_ACTION_BUTTON);
        salesSummaryButton.setFont(Font.MAIN_ACTTION_BUTTON);
        salesSummaryButton.setMinimumSize(Size.TAB_ACTION_BUTTON);
        salesSummaryButton.setPreferredSize(Size.TAB_ACTION_BUTTON);
        salesSummaryButton.setMaximumSize(Size.TAB_ACTION_BUTTON);
        salesSummaryButton.setBackground(Color.MENU_BUTTON_BACKGROUND);

        body = new JPanel();
        body.setLayout(new CardLayout());
        bodyLayoutManager = (CardLayout)body.getLayout();

        setTitle(title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        setSize(Size.WINDOW_RESOLUTION_DEFAULT_WIDTH, Size.WINDOW_RESOLUTION_DEFAULT_HEIGHT);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setLocationRelativeTo(null);
        add(createHeader(orderMenuButton, salesSummaryButton), BorderLayout.NORTH);
        add(body, BorderLayout.CENTER);
    }

    private JPanel createHeader(Button orderMenButton, Button salesSummaryButton) {
        JPanel header = new JPanel();
        header.setPreferredSize(new Dimension(100, 76));
        header.setBackground(Color.HEADER_BACKGROUND);
        header.setLayout(new FlowLayout(FlowLayout.LEFT, 19, 19));
        header.add(orderMenuButton);
        header.add(salesSummaryButton);

        return header;
    }

    public void addBody(JPanel body, String keyword) {
        body.setBackground(Color.BODY_BACKGROUND);
        this.body.add(body, keyword);
    }

    public void setSelectedBody(String keyword) {
        bodyLayoutManager.show(this.body, keyword);
    }
}
