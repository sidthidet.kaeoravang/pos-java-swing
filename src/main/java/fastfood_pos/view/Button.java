package fastfood_pos.view;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JButton;

import lombok.Getter;

@Getter
public class Button extends JButton {

    public Button(String label, Font font, Dimension size) {
        super(label);

        setMinimumSize(size);
        setPreferredSize(size);
        setMaximumSize(size);
        setFont(font);
        setFocusable(false);
    }
}