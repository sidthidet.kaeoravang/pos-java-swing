package fastfood_pos.view.order_menu_tab;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;

import fastfood_pos.configuration.Color;
import fastfood_pos.configuration.Font;
import fastfood_pos.model.ShoppingCartTableModel;

public class ShoppingCartTable extends JTable {
    ShoppingCartTableModel shoppingCartTableModel;

    ShoppingCartTable() {
        shoppingCartTableModel = new ShoppingCartTableModel();

        setModel(shoppingCartTableModel);
        setFont(Font.SHOPPING_CART_TABLE_BODY);
        setFocusable(false);
        setOpaque(true);
        setFillsViewportHeight(true);
        setBackground(Color.BODY_BACKGROUND);
        setRowHeight(35);

        JTableHeader header = getTableHeader();
        header.setReorderingAllowed(false);
        header.setFont(Font.SHOPPING_CART_TABLE_HEADER);
        header.setBackground(Color.SHOPPING_CART_TABLE_HEADER_BACKGROUND);
        header.setForeground(Color.SHOPPING_CART_TABLE_HEADER_FOREGROUND);
        header.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.SHOPPING_CART_TABLE_HEADER_BACKGROUND));

        DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
        leftRenderer.setHorizontalAlignment(JLabel.LEFT);

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        TableColumnModel columnModel = getColumnModel();
        columnModel.getColumn(0).setCellRenderer(leftRenderer);

        columnModel.getColumn(1).setCellRenderer(centerRenderer);
        columnModel.getColumn(1).setPreferredWidth(90);
        columnModel.getColumn(1).setMaxWidth(90);

        columnModel.getColumn(2).setCellRenderer(rightRenderer);
        columnModel.getColumn(2).setPreferredWidth(130);
        columnModel.getColumn(2).setMaxWidth(130);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    @Override
    public ShoppingCartTableModel getModel() {
        return this.shoppingCartTableModel;
    }

}
