package fastfood_pos.view.order_menu_tab;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fastfood_pos.configuration.Color;
import fastfood_pos.configuration.Font;
import fastfood_pos.view.TitleAndMoneyValuePanel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentSummaryView extends JPanel {
    private TitleAndMoneyValuePanel totalPrice;
    private TitleAndMoneyValuePanel discount;
    private TitleAndMoneyValuePanel vat;
    private TitleAndMoneyValuePanel netTotalPrice;
    private TitleAndMoneyValuePanel change;
    private JTextField moneyInput;
    private JButton payOrConFirmOrFinishButton;

    private static Dimension summaryRowPreferredSize = new Dimension(447, 30);

    PaymentSummaryView() {
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        setBorder(BorderFactory.createEmptyBorder(19, 19, 19, 19));
        setBackground(Color.BODY_BACKGROUND);

        totalPrice = new TitleAndMoneyValuePanel("ราคารวม", 0.0);
        discount = new TitleAndMoneyValuePanel("ส่วนลด(10%)", 0.0);
        vat = new TitleAndMoneyValuePanel("ภาษีมูลค่าเพิ่ม(7%)", 0.0);
        netTotalPrice = new TitleAndMoneyValuePanel("ราคาสุทธิ", 0.0);
        change = new TitleAndMoneyValuePanel("เงินที่ต้องทอน", 0.0);

        add(totalPrice);
        add(discount);
        add(vat);
        add(netTotalPrice);
        add(change);
        add(Box.createVerticalGlue());
        add(createInputChangePanel());
    }

    private JPanel createInputChangePanel() {
        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.LINE_AXIS));
        inputPanel.setPreferredSize(summaryRowPreferredSize);
        inputPanel.setBackground(Color.BODY_BACKGROUND);
        
        moneyInput = new JTextField();
        moneyInput.setPreferredSize(new Dimension(324, 46));
        moneyInput.setMaximumSize(new Dimension(Integer.MAX_VALUE, 46));
        moneyInput.setAlignmentY(Component.CENTER_ALIGNMENT);
        moneyInput.setFont(Font.PAYMENT_SUMMARY_ROW);

        payOrConFirmOrFinishButton = new JButton("ชำระเงิน");
        payOrConFirmOrFinishButton.setFocusable(false);
        payOrConFirmOrFinishButton.setMinimumSize(new Dimension(114, 46));
        payOrConFirmOrFinishButton.setPreferredSize(new Dimension(114, 46));
        payOrConFirmOrFinishButton.setMaximumSize(new Dimension(114, 46));
        payOrConFirmOrFinishButton.setAlignmentY(Component.CENTER_ALIGNMENT);
        payOrConFirmOrFinishButton.setFont(Font.PAYMENT_SUMMARY_ROW);
        payOrConFirmOrFinishButton.setForeground(Color.SHOPPING_CART_ACTION_BUTTON_FOREGROUND);
        payOrConFirmOrFinishButton.setBackground(Color.SHOPPING_CART_ACTION_BUTTON_BACKGROUND);

        inputPanel.add(moneyInput);
        inputPanel.add(Box.createRigidArea(new Dimension(8, 0)));
        inputPanel.add(Box.createHorizontalGlue());
        inputPanel.add(payOrConFirmOrFinishButton);
        return inputPanel;
    }
}