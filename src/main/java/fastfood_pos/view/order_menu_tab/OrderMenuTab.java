package fastfood_pos.view.order_menu_tab;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import fastfood_pos.configuration.Color;
import fastfood_pos.view.MinimalScrollPane;
import lombok.Getter;

@Getter
public class OrderMenuTab extends JPanel {
    private ShoppingCartActionButton shoppingCartActionButton;
    private ShoppingCartTable shoppingCartTable;
    private PaymentSummaryView paymentSummary;
    private ProductList productList;

    public OrderMenuTab() {
        shoppingCartTable = new ShoppingCartTable();
        paymentSummary = new PaymentSummaryView();
        shoppingCartActionButton = createShoppingCartActionButton();
        productList = new ProductList();

        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        setBorder(BorderFactory.createEmptyBorder(19, 19, 19, 19));
        add(createOrderDetailsPanel(shoppingCartTable, paymentSummary));
        add(Box.createRigidArea(new Dimension(23, 0)));
        add(shoppingCartActionButton);
        add(Box.createRigidArea(new Dimension(23, 0)));
        add(createProductListScrollPane(productList));
    }

    private JPanel createOrderDetailsPanel(ShoppingCartTable shoppingCartTable, PaymentSummaryView paymentSummary) {
        JPanel orderDetailsPane = new JPanel();
        orderDetailsPane.setLayout(new BoxLayout(orderDetailsPane, BoxLayout.PAGE_AXIS));
        orderDetailsPane.setPreferredSize(new Dimension(484, 716));
        orderDetailsPane.setMaximumSize(new Dimension(484, Integer.MAX_VALUE));
        orderDetailsPane.setAlignmentY(Component.TOP_ALIGNMENT);
        orderDetailsPane.setBackground(Color.BODY_BACKGROUND);

        MinimalScrollPane orderAllProductsScrollPane = new MinimalScrollPane(shoppingCartTable);
        orderAllProductsScrollPane.setPreferredSize(new Dimension(484, 375));

        paymentSummary.setPreferredSize(new Dimension(484, 322));
        paymentSummary.setMaximumSize(new Dimension(484, 322));

        orderDetailsPane.add(orderAllProductsScrollPane);
        orderDetailsPane.add(Box.createRigidArea(new Dimension(0, 19)));
        orderDetailsPane.add(paymentSummary);
        return orderDetailsPane;
    }

    private ShoppingCartActionButton createShoppingCartActionButton() {
        ShoppingCartActionButton shoppingCartActionButton = new ShoppingCartActionButton();
        shoppingCartActionButton.setPreferredSize(new Dimension(124, 200));
        shoppingCartActionButton.setMaximumSize(new Dimension(124, 200));
        shoppingCartActionButton.setAlignmentY(Component.TOP_ALIGNMENT);
        return shoppingCartActionButton;
    }

    private JScrollPane createProductListScrollPane(ProductList productList) {
        MinimalScrollPane scrollPane = new MinimalScrollPane(productList);
        scrollPane.setPreferredSize(new Dimension(750, 716));
        scrollPane.setAlignmentY(Component.TOP_ALIGNMENT);
        return scrollPane;
    }
}
