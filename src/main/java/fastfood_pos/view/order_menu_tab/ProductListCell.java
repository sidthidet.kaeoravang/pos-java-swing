package fastfood_pos.view.order_menu_tab;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.text.NumberFormat;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fastfood_pos.configuration.Color;
import fastfood_pos.configuration.Font;
import fastfood_pos.configuration.Size;
import fastfood_pos.model.ProductModel;
import lombok.Getter;

public class ProductListCell extends JPanel {
    @Getter
    private ProductModel product;
    JPanel innerPane;
    private static Dimension imageSize = new Dimension(Size.PRODUCT_LIST_CELL_WIDTH,
            Size.PRODUCT_LIST_CELL_HEIGHT - 35);
    private static NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("th", "TH"));

    ProductListCell(ProductModel product) {
        this.product = product;
        innerPane = new JPanel();
        innerPane.setPreferredSize(Size.PRODUCT_LIST_CELL);
        innerPane.setMaximumSize(Size.PRODUCT_LIST_CELL);
        innerPane.setLayout(new BoxLayout(innerPane, BoxLayout.PAGE_AXIS));
        innerPane.setBackground(Color.BODY_BACKGROUND);
        innerPane.add(createImageLabel(product.getImagePath()));
        innerPane.add(Box.createVerticalGlue());
        innerPane.add(createNameAndPricePane(product.getName(), product.getPrice()));

        setLayout(new BorderLayout());
        setBorder(BorderFactory.createMatteBorder(0, 0, 19, 19, Color.BODY_BACKGROUND));
        add(innerPane, BorderLayout.CENTER);
    }

    private static JLabel createImageLabel(String relativePath) {
        JLabel imageLabel = new JLabel();

        imageLabel.setMinimumSize(imageSize);
        imageLabel.setPreferredSize(imageSize);
        imageLabel.setMaximumSize(imageSize);
        imageLabel.setIcon(createImageIcon(relativePath));
        imageLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        return imageLabel;
    }

    private static JPanel createNameAndPricePane(String name, Double price) {

        JPanel namePriceAndPane = new JPanel();
        namePriceAndPane.setLayout(new BoxLayout(namePriceAndPane, BoxLayout.LINE_AXIS));
        namePriceAndPane.setBackground(Color.BODY_BACKGROUND);

        JLabel nameLabel = new JLabel();
        nameLabel.setText(name);
        nameLabel.setFont(Font.PRODUCT_LIST);

        JLabel priceLabel = new JLabel();
        priceLabel.setText(formatter.format(price));
        priceLabel.setFont(Font.PRODUCT_LIST);

        namePriceAndPane.add(nameLabel);
        namePriceAndPane.add(Box.createHorizontalGlue());
        namePriceAndPane.add(priceLabel);
        return namePriceAndPane;
    }

    private static ImageIcon createImageIcon(String path) {
        URL imageURL = ClassLoader.getSystemResource(path);
        if (imageURL != null) {
            try {
                BufferedImage image = ImageIO.read(imageURL.openStream());
                Image imageNewSize = image.getScaledInstance(Size.PRODUCT_LIST_CELL_WIDTH,
                        Size.PRODUCT_LIST_CELL_HEIGHT - 35, Image.SCALE_SMOOTH);
                return new ImageIcon(imageNewSize);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
