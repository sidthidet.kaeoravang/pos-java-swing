package fastfood_pos.view.order_menu_tab;

import java.awt.Component;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JList;

import fastfood_pos.configuration.Color;
import fastfood_pos.model.ProductModel;
import lombok.Getter;

public class ProductList extends JList<ProductListCell> {
    @Getter
    private DefaultListModel<ProductListCell> model;

    public ProductList() {
        model = new DefaultListModel<ProductListCell>();
        setModel(model);
        setCellRenderer(new ProductListCellRender());
        setLayoutOrientation(JList.HORIZONTAL_WRAP);
        setVisibleRowCount(-1);
        setBorder(BorderFactory.createEmptyBorder(19, 19, 0, 0));
    }

    public void addAllProducts(List<ProductModel> products) {
        for (ProductModel product : products) {
            model.addElement(new ProductListCell(product));
        }
    }
}

class ProductListCellRender extends DefaultListCellRenderer {
    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
            boolean cellHasFocus) {
        ProductListCell productListCell = (ProductListCell) value;

        if (isSelected) {
            productListCell.innerPane.setBorder(
                    BorderFactory.createMatteBorder(2, 2, 2, 2, Color.PRODUCT_LIST_BORDER_SELECTED_CELL));
        } else {
            productListCell.innerPane
                    .setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.PRODUCT_LIST_BORDER_CELL));
        }

        return productListCell;
    }

}