package fastfood_pos.view.order_menu_tab;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

import fastfood_pos.configuration.Color;
import fastfood_pos.configuration.Font;
import fastfood_pos.configuration.Size;
import fastfood_pos.view.Button;
import lombok.Getter;

@Getter
public class ShoppingCartActionButton extends JPanel {
        private Button addButton;
        private Button decreaseButton;
        private Button removeButton;

        private static Dimension spaceBetweenButtons = new Dimension(0, 16);

        ShoppingCartActionButton() {
                addButton = new Button("เพิ่ม <<", Font.SHOPPING_CART_ACTTION_BUTTON,
                                Size.ORDER_PRODUCT_ACTTION_BUTTON);
                addButton.setBackground(Color.SHOPPING_CART_ACTION_BUTTON_BACKGROUND);
                addButton.setForeground(Color.SHOPPING_CART_ACTION_BUTTON_FOREGROUND);
                addButton.setAlignmentX(Component.CENTER_ALIGNMENT);

                decreaseButton = new Button("ลด >>", Font.SHOPPING_CART_ACTTION_BUTTON,
                                Size.ORDER_PRODUCT_ACTTION_BUTTON);
                decreaseButton.setBackground(Color.SHOPPING_CART_ACTION_BUTTON_BACKGROUND);
                decreaseButton.setForeground(Color.SHOPPING_CART_ACTION_BUTTON_FOREGROUND);
                decreaseButton.setAlignmentX(Component.CENTER_ALIGNMENT);

                removeButton = new Button("นำออก x", Font.SHOPPING_CART_ACTTION_BUTTON,
                                Size.ORDER_PRODUCT_ACTTION_BUTTON);
                removeButton.setBackground(Color.SHOPPING_CART_ACTION_BUTTON_BACKGROUND);
                removeButton.setForeground(Color.SHOPPING_CART_ACTION_BUTTON_FOREGROUND);
                removeButton.setAlignmentX(Component.CENTER_ALIGNMENT);

                setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
                add(Box.createVerticalGlue());
                add(addButton);
                add(Box.createRigidArea(spaceBetweenButtons));
                add(decreaseButton);
                add(Box.createRigidArea(spaceBetweenButtons));
                add(removeButton);
                setBackground(Color.BODY_BACKGROUND);
        }
}
