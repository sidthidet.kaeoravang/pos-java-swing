package fastfood_pos.view.sales_summary_tab;

import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import fastfood_pos.configuration.Color;
import fastfood_pos.configuration.Font;
import fastfood_pos.configuration.Size;
import fastfood_pos.model.SummaryProductTableModel;

public class SummaryProductTable extends JTable {
    private SummaryProductTableModel model;

    public SummaryProductTable() {
        model = new SummaryProductTableModel();
        setModel(model);
        getTableHeader().setFont(Font.SUMMARY_PRODUCT_TABLE_HEADER);
        setFont(Font.SUMMARY_PRODUCT_TABLE_BODY);
        setRowHeight(Size.SUMMARY_PRODUCT_TABLE_CELL_HEIGHT);
        setDefaultRenderer(JLabel.class, new TableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                    boolean hasFocus,
                    int row, int column) {
                return (JLabel) value;
            }

        });

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        TableColumn imageColumn =  getColumnModel().getColumn(0);
        imageColumn.setPreferredWidth(Size.SUMMARY_PRODUCT_TABLE_CELL_WIDTH);
        imageColumn.setMaxWidth(Size.SUMMARY_PRODUCT_TABLE_CELL_WIDTH);

        setDefaultRenderer(String.class, centerRenderer);
        setDefaultRenderer(Double.class, centerRenderer);
        setDefaultRenderer(Integer.class, centerRenderer);
        setRowSelectionAllowed(false);
        setOpaque(true);
        setFillsViewportHeight(true);
        setBackground(Color.BODY_BACKGROUND);

        JTableHeader header = getTableHeader();
        header.setReorderingAllowed(false);
        header.setFont(Font.SHOPPING_CART_TABLE_HEADER);
        header.setBackground(Color.SUMMARY_PRODUCT_TABLE_HEAD_BACKGROUND);
        header.setForeground(Color.SUMMARY_PRODUCT_TABLE_HEAD_FOREGROUND);
        header.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.SUMMARY_PRODUCT_TABLE_HEAD_BACKGROUND));
    }

    @Override
    public SummaryProductTableModel getModel() {
        return model;
    }

}
