package fastfood_pos.view.sales_summary_tab;

import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import fastfood_pos.configuration.Color;
import fastfood_pos.view.TitleAndMoneyValuePanel;

public class SalesSummaryPanel extends JPanel {
    private TitleAndMoneyValuePanel totalSales;
    private TitleAndMoneyValuePanel discount;
    private TitleAndMoneyValuePanel vat;
    private TitleAndMoneyValuePanel revenue;

    SalesSummaryPanel() {
        setLayout(new GridLayout(2, 2, 200, 19));
        setBorder(BorderFactory.createEmptyBorder(19, 19, 19, 19));
        setBackground(Color.SALES_SUMMARY_BACKGROUND);

        totalSales = new TitleAndMoneyValuePanel("ยอดขายรวม", 0.0);

        discount = new TitleAndMoneyValuePanel("ส่วนลด", 0.0);
        vat = new TitleAndMoneyValuePanel("ภาษีมูลค่าเพิ่ม(7%)", 0.0);
        revenue = new TitleAndMoneyValuePanel("รายรับ(รวมภาษี)", 0.0);
        
        add(totalSales);
        add(vat);
        add(discount);
        add(revenue);
    }

    public Double getTotalSales() {
        return totalSales.getValue();
    }

    public void setTotalSales(Double totalSales) {
        this.totalSales.setValue(totalSales);
    }

    public Double getDiscount() {
        return discount.getValue();
    }

    public void setDiscount(Double discount) {
        this.discount.setValue(discount);
    }

    public Double getVat() {
        return vat.getValue();
    }

    public void setVat(Double vat) {
        this.vat.setValue(vat);
    }

    public Double getRevenue() {
        return revenue.getValue();
    }

    public void setRevenue(Double revenue) {
        this.revenue.setValue(revenue);
    }
    
}
