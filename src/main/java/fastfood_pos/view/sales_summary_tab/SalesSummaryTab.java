package fastfood_pos.view.sales_summary_tab;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

import fastfood_pos.model.SummaryProductTableModel;
import fastfood_pos.view.MinimalScrollPane;
import lombok.Getter;

@Getter
public class SalesSummaryTab extends JPanel {
    private SalesSummaryPanel salesSummary;
    private SummaryProductTable productSummaryTable;
    private SummaryProductTableModel productSummaryTableModel;

    public SalesSummaryTab() {
        salesSummary = new SalesSummaryPanel();
        salesSummary.setMaximumSize(new Dimension(Integer.MAX_VALUE, 116));

        productSummaryTable = new SummaryProductTable();
        productSummaryTableModel = productSummaryTable.getModel();
        

        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        setBorder(BorderFactory.createEmptyBorder(0, 19, 19, 19));
        add(salesSummary);
        add(Box.createRigidArea(new Dimension(0, 19)));
        add(new MinimalScrollPane(productSummaryTable));
    }
}
