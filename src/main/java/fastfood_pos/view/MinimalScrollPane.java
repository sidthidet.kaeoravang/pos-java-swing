package fastfood_pos.view;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.plaf.basic.BasicScrollBarUI;

import fastfood_pos.configuration.Color;

public class MinimalScrollPane extends JScrollPane{
    public MinimalScrollPane(Component view) {
        setViewportView(view);
        setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BODER_LINE));
        getVerticalScrollBar().setBackground(Color.MODERN_SCROLL_PANE_BACKGROUND);
        getVerticalScrollBar().setUI(new BasicScrollBarUI() {
            @Override
            protected void configureScrollBarColors() {
                this.thumbColor = Color.MODERN_SCROLL_PANE_FOREGROUND;
            }

            @Override
            protected JButton createDecreaseButton(int orientation) {
                return createZeroButton();
            }

            @Override
            protected JButton createIncreaseButton(int orientation) {
                return createZeroButton();
            }

            private JButton createZeroButton() {
                JButton jbutton = new JButton();
                jbutton.setPreferredSize(new Dimension(0, 0));
                jbutton.setMinimumSize(new Dimension(0, 0));
                jbutton.setMaximumSize(new Dimension(0, 0));
                return jbutton;
            }
        });
    }
}
