package fastfood_pos.view;

import java.awt.Dimension;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fastfood_pos.configuration.Color;
import fastfood_pos.configuration.Font;

public class TitleAndMoneyValuePanel extends JPanel {
    private JLabel valueLabel;
    private Double value;
    private static Dimension summaryRowPreferredSize = new Dimension(447, 30);
    NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("th", "TH"));

    public TitleAndMoneyValuePanel(String title, Double value) {
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        setPreferredSize(summaryRowPreferredSize);
        setBackground(Color.BODY_BACKGROUND);
        setForeground(Color.BODY_FOREGROUND);

        JLabel titleLabel = new JLabel(title);
        titleLabel.setFont(Font.TITLE_AND_MONEY_VALUE_PANEL);

        this.value = value;
        valueLabel = new JLabel(formatter.format(value));
        valueLabel.setFont(Font.TITLE_AND_MONEY_VALUE_PANEL);

        add(titleLabel);
        add(Box.createGlue());
        add(valueLabel);
    }

    public void setValue(Double value) {
        this.value = value;
        valueLabel.setText(formatter.format(value));
    }

    public Double getValue() {
        return this.value;
    }
}
