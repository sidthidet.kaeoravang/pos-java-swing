## ตัวอย่างหน้าจอของโปรแกรม
#### เมนูสั่งซื้อ
![order menu](screenshot/order_menu.png "เมนูสั่งซื้อ")
#### สรุปยอดขาย
![sales summary](screenshot/sales_summary.png "สรุปยอดขาย")
## สิ่งที่ต้องติดตั้งก่อน
- [OpenJDK 17](https://www.oracle.com/java/technologies/downloads/#jdk17-windows)
- [Maven](https://maven.apache.org/)
- [MariaDB](https://mariadb.org/) หรือ [MySQL](https://www.mysql.com/) หรือ MySQL บน [XAMPP](https://www.apachefriends.org/download.html)
## ตั้งค่าเพื่อติดต่อกับฐานข้อมูล
1. สร้างฐานข้อมูลตามต้องการ
2. เข้าไปในโฟลเดอร์ `src/main/resources/` จากนั้นให้เปลี่ยนชื่อหรือคัดลอกไฟล์ `hibernate.cfg.xml.example` ไปเป็น `hibernate.cfg.xml` (ตัดส่วน .example ออก)
3. เปิดไฟล์ `src/main/resources/hibernate.cfg.xml` เพื่อกำหนดค่าสำหรับเชื่อมต่อฐานข้อมูล\
โดยในไฟล์มีตัวอย่างซึ่งเป็นการเชื่อมต่อไปยังฐานข้อมูลที่ชื่อ database โดยผู้ใช้ที่ชื่อ root และมีรหัสผ่านคือ password
    - กำหนดชื่อฐานข้อมูลที่ต้องการ: `<property name="connection.url">jdbc:mysql://localhost:3306/database</property>`
    - กำหนดชื่อผู้ใช้งานฐานข้อมูล: `<property name="connection.username">root</property>`
    - กำหนดรหัสผ่านของผู้ใช้  `<property name="connection.password">password</property>`
## รันโปรแกรม
แนะนำให้ใช้โปรแกรม [VSCode](https://code.visualstudio.com/) โดยเมื่อเปิดโปรเจคท์จะมีส่วนเสริม(extension)ขึ้นมาแนะนำให้ติดตั้งคือ [Extensions Pack for Java](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack) และ [Lombok](https://marketplace.visualstudio.com/items?itemName=GabrielBB.vscode-lombok)\
เมื่อติดตั้งส่วนเสริมเสร็จสิ้นแล้วให้รีสตาร์ทโปรแกรม VSCode จากนั้นจึงจะสามารถรันโปรแกรมได้(แนะนำให้กดปุ่ม V ที่อยู่ข้างตัวรันแล้วเลือก run java)
